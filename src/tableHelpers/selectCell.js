/// get the indexes of dragged rows
export const draggingRows = (row, firstSelection, selectedData) => {
    let { rows } = selectedData

    if (rows.includes(row)) {
        if (rows.length > firstSelection.rows.length) {
            // remove by going up
            if (row >= firstSelection.rows[firstSelection.rows.length - 1]) {
                let index = rows.indexOf(row);
                if (index > -1) {
                    rows.splice(index + 1, 1);
                }
            }

            // remove by dragging down
            if (row <= firstSelection.rows[0]) {
                let index = rows.indexOf(row - 1);
                if (index > -1) {
                    rows.splice(index, 1);
                }
            }
        }
    } else {
        // add rows down -> push row at the end
        if (row > rows[rows.length - 1]) {
            rows.push(row);
        }
        // add rows up -> at the index 0
        if (row < rows[0]) {
            rows.insert(0, row);
        }
    }
}

// get the ibdexes of dragged cols
export const draggingCols = (col, firstSelection, selectedData) => {
    let { cols } = selectedData;

    if (cols.includes(col)) {
        if (col < cols[cols.length - 1]) {
            if (!firstSelection.cols.includes(col)) {
                // remove from left
                if (col > firstSelection.cols[firstSelection.cols.length - 1]) {
                    let index = cols.indexOf(col);
                    if (index > 0) {
                        cols.splice(index + 1, 1);
                    }
                }

                // remove from right
                if (col < firstSelection.cols[0]) {
                    let index = cols.indexOf(col - 1);
                    if (index >= 0) {
                        cols.splice(index, 1);
                    }
                }
            }

            if (col == firstSelection.cols[firstSelection.cols.length - 1]) {
                let index = cols.indexOf(col);
                if (index >= 0) {
                    cols.splice(index + 1, 1);
                }
            }
        }

        if (col == firstSelection.cols[0]) {
            if (cols.length > firstSelection.cols.length) {
                let index = cols.indexOf(col - 1);
                cols.splice(index, 1);
            }
        }
    } else {
        // add right: push at the end
        if (col > cols[cols.length - 1]) {
            cols.push(col);
        }

        // add left: push at index 0
        if (col < cols[0]) {
            cols.insert(0, col);
        }
    }
}