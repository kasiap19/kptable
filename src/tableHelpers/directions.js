export const bottomRight = (secondSelectionValues, firstSelColsLength, firstSelRowsLenth) => {
    // RIGHT, BOTTOM-RIGHT, Down
    // x > 0, y < 0
    const colLength = secondSelectionValues.value[0].length;
    for (let ic = 0; ic < colLength; ic++) {
        for (let ir = 0; ir < secondSelectionValues.value.length; ir++) {
            let firstSelectionColumns;
            let firstSelectionRows;

            ic > firstSelColsLength.value - 1 === false
                ? (firstSelectionColumns = 0)
                : (firstSelectionColumns = firstSelColsLength.value);
            ir > firstSelRowsLenth.value - 1 === false
                ? (firstSelectionRows = 0)
                : (firstSelectionRows = firstSelRowsLenth.value);

            const x = ir - firstSelectionRows;
            const y = ic - firstSelectionColumns;

            secondSelectionValues.value[ir][ic] = secondSelectionValues.value[x][y];
        }
    }
    return secondSelectionValues.value
}

export const bottomLeft = (secondSelectionValues, firstSelection, selectedData,firstSelColsLength, firstSelRowsLenth) => {
    // x_pos < 0 , y_pos > 0
    const colLength = secondSelectionValues.value[0].length;
    const firstSelRowsSize = firstSelection.rows.length;
    const firstSelLastRow = firstSelection.rows[firstSelRowsSize - 1];
    const firstSelFirstCol = firstSelection.cols[0];

    for (let ic = colLength - 1; ic >= 0; ic--) {
        for (let ir = 0; ir < secondSelectionValues.value.length; ir++) {
            let firstSelectionColumns;
            let firstSelectionRows;

            const secondSelCol = selectedData.cols[ic];
            const secondSelRow = selectedData.rows[ir];

            const xPos = secondSelCol - firstSelFirstCol;
            const yPos = secondSelRow - firstSelLastRow;

            xPos < 0
            ? (firstSelectionColumns = firstSelColsLength.value)
            : (firstSelectionColumns = 0);
            yPos > 0
            ? (firstSelectionRows = firstSelRowsLenth.value)
            : (firstSelectionRows = 0);

            const x = ir - firstSelectionRows;
            const y = ic + firstSelectionColumns;

            secondSelectionValues.value[ir][ic] =
            secondSelectionValues.value[x][y];
        }
    }

    return secondSelectionValues.value
}

export const topLeft = (secondSelectionValues, firstSelection, selectedData, firstSelColsLength, firstSelRowsLenth) => {
    // x_pos < 0 , y_pos < 0
    const colSecondSelLength = secondSelectionValues.value[0].length;
    const rowSecondSelLength = selectedData.rows.length;

    const firstSelFirstRow = firstSelection.rows[0];
    const firstSelFirstCol = firstSelection.cols[0];

    for (let ic = colSecondSelLength - 1; ic >= 0; ic--) {
      for (let ir = rowSecondSelLength - 1; ir >= 0; ir--) {
        let firstSelectionColumns;
        let firstSelectionRows;

        const secondSelCol = selectedData.cols[ic];
        const secondSelRow = selectedData.rows[ir];

        const xPos = secondSelCol - firstSelFirstCol;
        const yPos = secondSelRow - firstSelFirstRow;

        xPos < 0
          ? (firstSelectionColumns = firstSelColsLength.value)
          : (firstSelectionColumns = 0);
        yPos < 0
          ? (firstSelectionRows = firstSelRowsLenth.value)
          : (firstSelectionRows = 0);

        const x = ir + firstSelectionRows;
        const y = ic + firstSelectionColumns;

        secondSelectionValues.value[ir][ic] =
          secondSelectionValues.value[x][y];
      }
    }

    return secondSelectionValues.value
}

export const topRight = (secondSelectionValues, firstSelection, selectedData, firstSelColsLength, firstSelRowsLenth) => {
    // x_pos > 0 , y_pos < 0
    const colLength = secondSelectionValues.value[0].length;
    const rowsSecondSelLength = selectedData.rows.length;

    const firstSelFirstRow = firstSelection.rows[0];
    const firstSelLastCol = firstSelection.cols[firstSelColsLength.value - 1];

    for (let ic = 0; ic < colLength; ic++) {
      for (let ir = rowsSecondSelLength - 1; ir >= 0; ir--) {
        let firstSelectionColumns;
        let firstSelectionRows;

        const secondSelCol = selectedData.cols[ic];
        const secondSelRow = selectedData.rows[ir];

        const xPos = secondSelCol - firstSelLastCol;
        const yPos = secondSelRow - firstSelFirstRow;

        xPos > 0
          ? (firstSelectionColumns = firstSelColsLength.value)
          : (firstSelectionColumns = 0);
        yPos < 0
          ? (firstSelectionRows = firstSelRowsLenth.value)
          : (firstSelectionRows = 0);

        const x = ir + firstSelectionRows;
        const y = ic - firstSelectionColumns;

        secondSelectionValues.value[ir][ic] =
          secondSelectionValues.value[x][y];
      }
    }
    return secondSelectionValues.value
}