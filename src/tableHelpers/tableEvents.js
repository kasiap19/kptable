export const copy = async (props, selectedData) => {
    let toCsv = [];

    // get values from selected rows and cols
    let { rows, cols } = selectedData;
    if (rows.length && cols.length) {
        const filteredRows = rows.map((row) => props.rows[row]);
        filteredRows.forEach((row) => {
            let temp = [];
            cols.forEach((col) => temp.push(row[col]));

            toCsv.push(temp);
        });

        const excelData = toCsv.map((lines) => lines.join("\t")).join("\n");

        try {
            await navigator.clipboard.writeText(excelData);
        } catch (er) {
            console.log(er);
        }
    }
}

export const paste = async (props, selectedData, inputValue) => {
    // paste data from excel
    let data = await navigator.clipboard.readText()

    let clipRows = data.split("\n");
    for (let i = 0; i < clipRows.length; i++) {
        clipRows[i] = clipRows[i].split(String.fromCharCode(9));
    }

    clipRows = clipRows.filter((item) => item != "");

    // convert string numbers -> if there is no value set empty
    clipRows.forEach((el) => {
        el.forEach((x, index, arr) => {
            let nb = Number(x);
            nb >= 0 ? (arr[index] = nb) : (arr[index] = "");
        });
    });

    let updateRows = JSON.parse(JSON.stringify(props.rows));

    const { rows, cols } = selectedData;

    // update selected cells with single value
    if (clipRows.length === 1 && clipRows[0].length === 1) {
        rows.forEach((row) => {
            cols.forEach((col) => {
                updateRows[row][col] = clipRows[0][0];
            });
        });
    }

    // find starting row and starting col
    let startIndexRow, startIndexCol;
    if (rows.length > 0) {
        startIndexRow = rows[0];
        startIndexCol = cols[0];
    }

    // update selected cells with copied values from excel
    clipRows.forEach((copyRow, copyIndex) => {
        copyRow.forEach((row, index) => {
            updateRows[startIndexRow + copyIndex][startIndexCol + index] = row;
        });
    });

    // evt.target.blur();
    inputValue.value = clipRows[0][0];

    return updateRows
}